const express = require('express'),
    router = express.Router(),
    request = require('request');


router.get('/', (req, res) => {
    res.send('Hola Mi mano')
    //res.json({"hola":"mundo"})
})

router.post('/registro', (req, res) => {
    if (req.body.captcha === undefined || req.body.captcha === '' || req.body.captcha === null) {
        return res.json({"success":"false", "msg":" Seleccione el captcha"})
    }
    const secret_key = '6LfruoQUAAAAAO58gEycH6wTo7woJD0fMMC78KnJ'

    // VERIFY URL
    const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`

    request(verifyUrl, (err, response, body) => {
        body = JSON.parse(body);
        console.log(body);

        // If not success
        if (body.success !== undefined && !body.success ) {
            return res.json({"success":"false","msg":"failed captcha"})
        }
        // If success
        return res.json({"success":"true","msg":" Captcha passed"})
    })
})

module.exports = router

const express = require('express'),
    morgan = require('morgan'),
    cors = require('cors'),
    rutas = require('./routes/routs.js');

const app = express()

//Middlewares
app.use(morgan('dev'))
    .use(cors())
    .use(express.json())

// Rutas
app.use('/inicio', rutas)

// Host config
app.listen(process.env.PORT || 3000, () => {
    console.log('servidor corriendo en el puerto 3000');
})



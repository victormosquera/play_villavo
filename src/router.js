import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import login from './views/login';
import registro from './views/registro';
import change from './views/change';

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/registro',
      name: 'registro',
      component: registro
    },
    {
      path: '/change',
      name: 'change',
      component:change
    },
  ]
})

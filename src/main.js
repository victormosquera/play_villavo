import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import firebase from 'firebase/app';
import {config} from './firebase'

Vue.config.productionTip = true

Vue.use(BootstrapVue);

new Vue({
  router,
  store,
  created() {
    firebase.initializeApp(config);
  },
  render: h => h(App)
}).$mount('#app')
